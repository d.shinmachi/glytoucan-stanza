class UserListStanza < TogoStanza::Stanza::Base

  @@prefix = [
    'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
    'PREFIX owl: <http://www.w3.org/2002/07/owl#>',
    'PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>',
    'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
    'PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
    'PREFIX dcterms:  <http://purl.org/dc/terms/>'
  ].join("\n")
  @@from = [
    'FROM <http://purl.jp/bio/12/glyco/glycan#>',
    'FROM <http://rdf.glytoucan.org/users>'
  ].join("\n")
  @@filters = {}

  property :list do |offset, limit, key, search|
    @@filters = {}
    if key == 'id'
      @@filters['id'] = "VALUES ?userID {#{search}}"
    elsif key == 'name'
      @@filters['name'] = "FILTER regex (?userName, '#{search}', 'i') ."
    elsif key == 'project'
      @@filters['project'] = "FILTER regex (?project, '#{search}', 'i') ."
    end

    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT DISTINCT ?userID ?userName ?project
      #{@@from}
      WHERE{
        ?user a foaf:Person .
        #{@@filters['id']}
        ?user dcterms:identifier ?userID .
        #{@@filters['name']}
        ?user foaf:name ?userName .
        OPTIONAL{
          ?user foaf:member ?db .
          ?db rdfs:label ?project .
        }
        #{@@filters['project']}
      }
      OFFSET #{offset}
      LIMIT #{limit}
    SPARQL

  end

  property :countquery do |key, search|
    @@filters = {}
    if key == 'id'
      @@filters['id'] = "VALUES ?userID {#{search}}"
    elsif key == 'name'
      @@filters['name'] = "FILTER regex (?userName, '#{search}', 'i') ."
    elsif key == 'project'
      @@filters['project'] = "FILTER regex (?project, '#{search}', 'i') ."
    end
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT (COUNT(DISTINCT ?userID) AS ?total)
      #{@@from}
      WHERE{
        ?user a foaf:Person .
        #{@@filters['id']}
        ?user dcterms:identifier ?userID .
        #{@@filters['name']}
        ?user foaf:name ?userName .
        OPTIONAL{
          ?user foaf:member ?db .
          ?db rdfs:label ?project .
          BIND(strafter(str(?db), 'http://purl.jp/bio/12/glyco/glycan#') AS ?dbID)
        }
        #{@@filters['project']}
      }
    SPARQL
  end
end
