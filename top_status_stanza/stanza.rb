class TopStatusStanza < TogoStanza::Stanza::Base

  @@prefix = [
    'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
    'PREFIX glycan:  <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>'
  ].join("\n")
  @@from = [
  'FROM <http://rdf.glytoucan.org/core>',
  'FROM <http://rdf.glytoucan.org/sequence/wurcs>',
  # 'FROM <http://rdf.glytoucan.org/ms/carbbank>',
  'FROM NAMED <http://rdf.glytoucan.org/type/monosaccharide>',
  'FROM <http://rdf.glytoucan.org/motif>'
  ].join("\n")

  property :totalcount do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT (COUNT(DISTINCT ?AccessionNumber) AS ?total)
      #{@@from}
      WHERE {
        # Accession Number
        ?glycan glytoucan:has_primary_id ?AccessionNumber .
        ?glycan glycan:has_glycosequence ?gseq .
        ?gseq glycan:has_sequence ?Seq .
        ?gseq glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
      }
    SPARQL
  end

  property :motifcount do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT (COUNT(DISTINCT ?MotifName) AS ?num)
      #{@@from}
      WHERE {
        ?glycan glycan:has_motif ?motif .
        ?motif rdfs:label ?MotifName .
      }
    SPARQL
  end

  property :monosaccharidecount do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT (COUNT(DISTINCT ?AccessionNumber) AS ?num)
      #{@@from}
      WHERE {
        GRAPH <http://rdf.glytoucan.org/type/monosaccharide>{
          ?glycan a glycan:monosaccharide .
          ?glycan glytoucan:has_primary_id ?AccessionNumber .
        }
      }
    SPARQL
  end

end
