class RelationPubchemStanza < TogoStanza::Stanza::Base

  require 'net/http'
  require 'uri'
  require 'json'

  @@item_labels = {}
  @@default_labels = {
    'cid' => 'CID',
    'sid' => 'SID'
  }

  property :pubchemSearch do |acc,lang|

    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
	PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
	PREFIX dcterms: <http://purl.org/dc/terms/>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

	SELECT DISTINCT ?cid ?cid_url ?sid ?sid_url
	FROM <http://rdf.glytoucan.org/core>
	FROM <http://rdf.glycoinfo.org/mapping/glytoucan/pubchem>
	WHERE{
    	VALUES ?AccessionNumber {"#{acc}"}
		?saccharide glytoucan:has_primary_id ?AccessionNumber .
		?saccharide glycan:has_resource_entry  ?cid_entry, ?sid_entry .

		?cid_entry skos:closeMatch ?cid_url.
		?cid_entry dcterms:identifier ?cid .

		?sid_entry skos:exactMatch ?sid_url .
		?sid_entry dcterms:identifier ?sid .
	}

    SPARQL
  end

  property :pubchemLabels do |acc,lang|

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    label_list = [{}]
    label_list.map{|entry|
      entry[:l_cid] = @@item_labels['cid']
      entry[:l_sid] = @@item_labels['sid']
      entry
    }

  end

end
