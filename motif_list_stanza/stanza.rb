class MotifListStanza < TogoStanza::Stanza::Base
  property :motifAll do |notation, page|
    @@notation = notation
    @@page = page
    res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#> 
    PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>

    SELECT DISTINCT ?MotifName ?AccessionNumber ?ReducingEnd  (count(distinct ?glycan) AS ?Frequency)
    FROM <http://rdf.glytoucan.org/core>
    FROM <http://rdf.glytoucan.org/motif>
    WHERE {
      ?glycan glycan:has_motif ?motif .
      ?motif glytoucan:has_primary_id ?AccessionNumber .
      ?motif glytoucan:is_reducing_end ?ReducingEnd_boolean .
      ?motif rdfs:label ?motifLabel .
      BIND((str(?motifLabel) AS ?MotifName))
      BIND((str(?ReducingEnd_boolean) AS ?ReducingEnd))
    } ORDER BY DESC(?Frequency)
    SPARQL

    res.map{|r|
      r[:notation] = @@notation;
      if (page == 'motif_list')
        r[:url] = "/Structures/Glycans/#{r[:AccessionNumber]}"
      else
        r[:url] = "/Structures/?motif=#{r[:MotifName].gsub(/\s/, '+')}"
      end
    }
    res

  end
end
