class RelationGlycomedbStanza < TogoStanza::Stanza::Base
  require 'net/http'
  require 'uri'
  require 'json'

  @@prefix = [
    'PREFIX dcterms: <http://purl.org/dc/terms/>',
    'PREFIX skos: <http://www.w3.org/2004/02/skos/core#>',
    'PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>'
  ].join("\n")
  @@from = [
    'FROM <http://rdf.glytoucan.org/core>',
    'FROM NAMED <http://rdf.glycoinfo.org/mapping/glytoucan/glycome-db>',
    'FROM NAMED <http://rdf.glycoinfo.org/glycome-db>'
  ].join("\n")

  @@item_labels = {}
  @@default_labels = {
    'glycomedb' => 'GlycomeDB',
    'carbbank' => 'Carbbank',
    'ccsd' => 'CCSD',
    'cfg' => 'CFG',
    'glycO' => 'GlycO',
    'linkToXmlPage' => 'link to xml page',
    'glycobaseLille' => 'Glycobase lille',
    'glycosciences.de' => 'GLYCOSCIENCES.de',
    'jcggdb' => 'JCGGDB',
    'kegg' => 'KEGG',
    'pdb' => 'PDB'
  }

  property :glycomedbSearch do |acc,lang|

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    @@glycome_query = [
      'VALUES ?AccessionNumber {"' + acc + '"}',
      '?glytoucan glytoucan:has_primary_id ?AccessionNumber.',
      'GRAPH <http://rdf.glycoinfo.org/mapping/glytoucan/glycome-db>{' ,
      '?glytoucan skos:exactMatch ?gdb .',
      '?gdb glycan:has_resource_entry ?gurl .',
      '?gurl dcterms:identifier ?gid .',
      '}'
    ].join("\n")

    res_glycomedb = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT DISTINCT ?AccessionNumber
      # GlycomeDB ID
        ?gurl ?gid
      #{@@from}
      WHERE{
        #{@@glycome_query}
      }
    SPARQL

    res_others = {}

    res_others['ccsd'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_carbbank .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> CFG
    res_others['cfg'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_cfg .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> GlycoO
    res_others['glycO'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
    	  GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_glyco .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> Glycobase lille
    res_others['base'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_glycobase_lille .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> GYCOSCIENCES.de
    res_others['gde'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_glycosciences_de .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> JCGGDB
    res_others['jcggdb'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_jcggdb .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> KEGG
    res_others['kegg'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_kegg .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #>>> PDB
    res_others['pdb'] = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?url ?id ?gid
    #{@@from}
    WHERE{
      #{@@glycome_query}
    	OPTIONAL{
      	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
      		?gdb glycan:has_resource_entry ?url .
      		?url glycan:in_glycan_database glycan:database_pdb .
      		?url dcterms:identifier ?id.
      	}
      }
    }
    SPARQL

    #####
    result = {}

    res_glycomedb.each do |val|
      result[val[:gid]] = {}
      result[val[:gid]][:tag_glycomedb] = "<a href=\"#{val[:gurl]}\" target=\"_blank\">#{val[:gid]}</a>"
    end

    res_others.each do |key,arr|
      tag_key = "tag_#{key}".to_sym
      arr = arr.sort_by{|val|val[:id].to_i}
      arr.each do |val|
        result[val[:gid]][tag_key] = '' if result[val[:gid]][tag_key].nil?
        result[val[:gid]][tag_key] += "<a href=\"#{val[:url]}\" target=\"_blank\">#{val[:id]}</a>\s"
      end
    end

    res_array = []
    result.each do |key,tmp|
      tmp[:l_glycomedb] = @@item_labels['glycomedb']
      tmp[:l_carbbank] = @@item_labels['carbbank']
      tmp[:l_ccsd] = @@item_labels['ccsd']
      tmp[:l_cfg] = @@item_labels['cfg']
      tmp[:l_glycO] = @@item_labels['glycO']
      tmp[:l_linkToXmlPage] = @@item_labels['linkToXmlPage']
      tmp[:l_glycobaseLille] = @@item_labels['glycobaseLille']
      tmp[:l_glycosciences_de] = @@item_labels['glycosciences.de']
      tmp[:l_jcggdb] = @@item_labels['jcggdb']
      tmp[:l_kegg] = @@item_labels['kegg']
      tmp[:l_pdb] = @@item_labels['pdb']
      res_array.push(tmp)
    end

    res_array
    #####

  end
end
