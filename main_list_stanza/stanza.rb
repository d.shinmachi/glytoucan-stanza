class MainListStanza < TogoStanza::Stanza::Base

  require 'net/http'
  require 'uri'
  require 'json'

  @@define_order = [
    'DEFINE sql:select-option "order"'
  ].join("\n")
  @@prefix = [
    'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
    'PREFIX glycan:  <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>'
  ].join("\n")
  @@from = [
  'FROM <http://rdf.glytoucan.org/core>',
  # http://rdf.glytoucan.org/mass is wurcs mass
  'FROM <http://rdf.glytoucan.org/mass>',
  'FROM <http://rdf.glytoucan.org/sequence/wurcs>',
  'FROM <http://rdf.glytoucan.org/sequence/glycoct>',
  'FROM <http://rdf.glytoucan.org/users>',
  'FROM <http://rdf.glytoucan.org/ms/carbbank>',
  # <http://rdf.glytoucan.org/ms/carbbank> is constructed from <htt://rdf.glytoucan.org/msdb>
  # condition
  ## monosaccharide_notation_scheme_carbbank
  ## primary_name "true" only
  ## did not select boolean of the trivial name
  ## ?ComponentName = alias name + substituent + position
  'FROM <http://rdf.glytoucan.org/motif>'
  ].join("\n")

  @@item_labels = {}
  @@default_labels = {
    'accessionNumber' => 'Accession Number',
    'mass' => 'Mass',
    'motif' => 'Motif',
    'contributionTime' => 'Contribution Time'
  }


  @@mass_filter = ''
  @@motif_filter = ''
  @@component_filter = ''
  @@database_filter = ''

  property :mainlist do |massmin,massmax,motif,monosaccharide,database,order,orderkey,offset,lang|

  if massmin.empty? || massmax.empty?
    @@mass_filter = ''
  elsif massmin == '-1' && massmax == '-1'
    @@mass_filter = ''
  else
    @@mass_filter = "FILTER ((?Mass >= #{massmin}) && (?Mass <= #{massmax}))"
  end

  if motif.empty?
    @@motif_filter = ''
  else
    tmp = []
    motif.split('__').each_with_index do |m,i|
      next if m.empty?
      tmp.push("?glycan glycan:has_motif ?motif#{i} .")
      tmp.push("?motif#{i} rdfs:label ?MotifName#{i} .")
      tmp.push("FILTER (contains(str(?MotifName#{i}), '#{m}'))")
      # ?MotifName is such as "Sialyl Lewis A"@en
    end
    @@motif_filter = tmp.join("\n")
  end

  if monosaccharide.empty?
    @@component_filter = ''
  else
    tmp = []
    monosaccharide.split('__').each_with_index do |m, i|
      next if m.empty?
      m.gsub!(/\+/, ' ');
      name,min_max = m.split('_Min_')
      min,max = min_max.split('_Max_')

      tmp.push("?glycan glycan:has_component ?comp#{i} .")
      tmp.push("?comp#{i} glycan:has_cardinality ?cardinality#{i} .")
      tmp.push("?comp#{i} glycan:has_monosaccharide ?mono#{i} .")
      tmp.push("FILTER ( (?cardinality#{i} >= #{min} ) && (?cardinality#{i} <= #{max}))")
      tmp.push("?mono#{i} glycan:has_alias ?msdb#{i} .")
      tmp.push("?msdb#{i} glycan:has_alias_name ?name#{i} .")
      tmp.push("FILTER (?name#{i} = '#{name}')")

    end
    @@component_filter = tmp.join("\n")
  end

  if database.empty?
    @@database_filter = ''
  else
    tmp = []
    database.split('__').each_with_index do |d, i|
      next if d.empty?
      d.gsub!(/\+/, ' ')
      tmp.push("{SELECT DISTINCT ?glycan")
      tmp.push("WHERE{")
      tmp.push("GRAPH ?g#{i} {")
      tmp.push("?glycan glycan:has_resource_entry ?res_db#{i}.")
      tmp.push("VALUES ?dbLabel#{i} { '#{d}' }")
      tmp.push("?res_db#{i} rdfs:label ?dbLabel#{i}.")
      tmp.push("}")
      tmp.push("}}")
    end
    @@database_filter = tmp.join("\n")
  end

  uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
  result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
  json = result.code == '200' || result.code == '304' ? result.body : '{"status": false}'
  list = JSON.parse(json)
  lang_replace = list['status'] == false ? {} : list['result']['common']
  @@default_labels.each do |key,label|
    @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
  end

  orderkey = orderkey.empty? ? 'AccessionNumber' : orderkey

    entry_list = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@define_order}
    #{@@prefix}
    SELECT DISTINCT ?AccessionNumber ?WURCSLabel ?GlycoCT ?Mass ?MassLabel ?ContributionTime 
    WHERE{
      {SELECT DISTINCT ?AccessionNumber ?WURCSLabel ?GlycoCT ?Mass ?MassLabel ?ContributionTime 
       #{@@from}
       WHERE{

        # Component
        #{@@component_filter}

        # Motifs
        #{@@motif_filter}

        # LinkedDB
        #{@@database_filter}

        # Accession Number
        ?glycan glytoucan:has_primary_id ?AccessionNumber .

        # Mass
        # a repeat structure don't have mass value
        OPTIONAL{
          ?glycan glytoucan:has_derivatized_mass ?dmass .
          ?dmass rdfs:label ?MassLabel .
          ?dmass glytoucan:has_mass ?Mass .
        }
        #{@@mass_filter}

        # ContributionTime
        OPTIONAL{
          ?glycan glycan:has_resource_entry ?res .
          ?res glytoucan:date_registered ?ContributionTime .
        }

        # WURCS
        OPTIONAL{
          ?glycan glycan:has_glycosequence ?wcsSeq .
          ?wcsSeq glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
          ?wcsSeq rdfs:label ?WURCSLabel .
        }

        # GlycoCT
        OPTIONAL{
          ?glycan glycan:has_glycosequence ?gctSeq .
          ?gctSeq glycan:in_carbohydrate_format glycan:carbohydrate_format_glycoct .
          ?gctSeq glycan:has_sequence ?GlycoCT .
        }

       }
        ORDER BY #{order}(?#{orderkey})
      }
    }
    OFFSET #{offset}
    LIMIT 20
  SPARQL

  entry_list.map{|entry|
    mass_is_num = true if Float(entry[:MassLabel]) rescue false
    entry[:MassLabel] = (entry[:MassLabel].to_f * 10000).round / 10000.0 if mass_is_num
    entry[:l_accessionNumber] = @@item_labels['accessionNumber']
    entry[:l_mass] = @@item_labels['mass']
    entry[:l_motif] = @@item_labels['motif']
    entry[:l_contributionTime] = @@item_labels['contributionTime']
    entry
  }
end

  # Count
  property :countquery do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@define_order}
    #{@@prefix}
    SELECT (COUNT(DISTINCT ?AccessionNumber) AS ?total)
    #{@@from}
    WHERE {
      # Accession Number
      ?glycan glytoucan:has_primary_id ?AccessionNumber .
      # WURCS
      ?glycan glycan:has_glycosequence ?gseq .
      ?gseq glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
      ?gseq glycan:has_sequence ?Seq .
      #{@@component_filter}
      #{@@motif_filter}
      #{@@database_filter}
      # Mass
      OPTIONAL{
        ?glycan glytoucan:has_derivatized_mass ?dmass .
        ?dmass glytoucan:has_mass ?Mass .
      }
      #{@@mass_filter}
    }
    SPARQL
  end

  property :massrange do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@define_order}
    #{@@prefix}
    SELECT (MIN(?Mass) as ?minMass) (MAX(?Mass) as ?maxMass)
    #{@@from}
    WHERE {
      #{@@component_filter}
      #{@@motif_filter}
      #{@@database_filter}
      # Mass
      # a repeat structure don't have mass value
      OPTIONAL{
        ?glycan glytoucan:has_derivatized_mass ?dmass .
        ?dmass glytoucan:has_mass ?Mass .
      }
      #{@@mass_filter}
    }
    SPARQL
  end

  # filter by motif list
  property :motiflist do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@define_order}
    #{@@prefix}
    SELECT ?MotifName (COUNT(DISTINCT ?AccessionNumber) AS ?num)
    #{@@from}
    WHERE {
      #{@@component_filter}
      # Motif
      ?glycan glycan:has_motif ?motif .
      ?motif rdfs:label ?MotifName .
      #{@@motif_filter}
      #{@@database_filter}
      # AccessionNumber
      ?glycan glytoucan:has_primary_id ?AccessionNumber .
      # Mass
      # a repeat structure don't have mass value
      OPTIONAL{
        ?glycan glytoucan:has_derivatized_mass ?dmass .
        ?dmass glytoucan:has_mass ?Mass .
      }
      #{@@mass_filter}
    } GROUP BY ?MotifName ORDER BY ?MotifName
    SPARQL
  end

  # filter by monosaccharide list
  property :componentlist do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@define_order}
    #{@@prefix}
    SELECT ?ComponentName (MIN(?cardinality) as ?MINcardinality) (MAX(?cardinality) as ?MAXcardinality) (COUNT(DISTINCT ?AccessionNumber) AS ?num)
    #{@@from}
    WHERE {
      # Component
      ?glycan glycan:has_component ?comp .
      ?comp glycan:has_cardinality ?cardinality .
      ?comp glycan:has_monosaccharide ?mono .
      ?mono glycan:has_alias ?msdb .
      ?msdb glycan:has_alias_name ?ComponentName .
      #{@@component_filter}
      #{@@motif_filter}
      #{@@database_filter}
      # AccessionNumber
      ?glycan glytoucan:has_primary_id ?AccessionNumber .
      # Mass
      OPTIONAL{
        ?glycan glytoucan:has_derivatized_mass ?dmass .
        ?dmass glytoucan:has_mass ?Mass .
      }
      #{@@mass_filter}
    } GROUP BY ?ComponentName ORDER BY ?ComponentName
    SPARQL
  end

  # filter by database list
  property :databaselist do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@define_order}
    #{@@prefix}
    SELECT ?dbLabel COUNT(DISTINCT ?AccessionNumber) AS ?num
    #{@@from}
    WHERE {
      #{@@component_filter}
      #{@@motif_filter}
      GRAPH ?g {
        ?glycan glycan:has_resource_entry ?res_db.
        ?res_db rdfs:label ?dbLabel.
      }
      #{@@database_filter}
      # AccessionNumber
      ?glycan glytoucan:has_primary_id ?AccessionNumber .
      # Mass
      OPTIONAL{
        ?glycan glytoucan:has_derivatized_mass ?dmass .
        ?dmass glytoucan:has_mass ?Mass .
      }
      #{@@mass_filter}
    } GROUP BY ?dbLabel ORDER BY ?dbLabel
    SPARQL
  end

end
