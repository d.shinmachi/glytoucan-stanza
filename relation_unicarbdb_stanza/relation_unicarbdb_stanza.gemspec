lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'relation_unicarbdb_stanza'
  spec.version       = '0.0.1'
  spec.authors       = ['Glytoucan project']
  spec.email         = ['glytoucan@gmail.com']
  spec.summary       = %q{Data visualization, analysis and searching tool for semantic web.}
  spec.description   = %q{relation_unicarbdb_stanza.}
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = Dir.glob('**/*')
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'togostanza'
end
