class RelationUnicarbdbStanza < TogoStanza::Stanza::Base

  require 'net/http'
  require 'uri'
  require 'json'

  @@item_labels = {}
  @@default_labels = {
    'unicarbdb_id' => 'UniCarb-DB ID'
  }

  property :unicarbdbSearch do |acc,lang|

    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX bibo: <http://purl.org/ontology/bibo/>
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#> 
    PREFIX glycodb: <http://purl.jp/bio/12/database/>
    PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>

  	SELECT DISTINCT ?unicarbdb_id ?url
  	FROM <http://rdf.glytoucan.org/core>
  	WHERE {

    	# Glytoucan 
      VALUES ?AccessionNumber {"#{acc}"}
    	?glytoucan glytoucan:has_primary_id ?AccessionNumber.
    	?glytoucan glycan:has_resource_entry ?entry .

    	# UniCarb-DB
    	?entry glycan:in_glycan_database glycan:database_unicarb_db .
    	?entry dcterms:identifier ?unicarbdb_id .
    	?entry rdfs:seeAlso ?url .
    	}
    SPARQL
  end

  property :unicarbdbLabels do |acc,lang|

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    label_list = [{}]
    label_list.map{|entry|
      entry[:l_unicarbdb_id] = @@item_labels['unicarbdb_id']
      entry
    }

  end

end
