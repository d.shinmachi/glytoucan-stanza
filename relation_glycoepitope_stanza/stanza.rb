class RelationGlycoepitopeStanza < TogoStanza::Stanza::Base
  require 'net/http'
  require 'uri'
  require 'json'

  @@prefix = [
    'DEFINE sql:select-option "order"',
    'PREFIX dcterms: <http://purl.org/dc/terms/>',
    'PREFIX bibo: <http://purl.org/ontology/bibo/>',
    'PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX glycodb: <http://purl.jp/bio/12/database/>',
    'PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>',
    'PREFIX glycoepitope: <http://www.glycoepitope.ac.jp/epitopes/glycoepitope.owl#>',
    'PREFIX glycoprot: <http://www.glycoprot.jp/>',
    'PREFIX uniprot: <http://www.uniprot.org/core/>'
  ].join("\n")
  @@from = [
    'FROM <http://rdf.glytoucan.org/core>',
    'FROM <http://rdf.glycoinfo.org/mapping/accnum/ep>',
    'FROM <http://rdf.glycoinfo.org/glycoepitope>'
  ].join("\n")

  @@item_labels = {}
  @@default_labels = {
    'epitope' => 'Epitope',
    'antibody' => 'Antibody',
    'epitopeId' => 'Epitope ID',
    'epitopeName' => 'Epitope name',
    'sequence' => 'Sequence',
    'glycoprotein' => 'Glycoprotein',
    'glycolipid' => 'Glycolipid',
    'cellName' => 'Cell line',
    'tissueName' => 'Tissue and Cellular distribution',
    'pubmed' => 'PubMed',
    'antibodyId' => 'Antibody ID',
    'antibodyName' => 'Antibody name'
  }

  property :glycoepitopeSearch do |acc,lang|

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    uri_res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{@@prefix}
      SELECT DISTINCT ?ep
      #{@@from}
      WHERE {
      	# Glytoucan
      	VALUES ?AccessionNumber {"#{acc}"}
      	?glytoucan glytoucan:has_primary_id ?AccessionNumber.

      	# Epitope uri 
      	?glytoucan glycan:has_epitope ?ep .
      }
    SPARQL

    res_array = []

    uri_res.each do |ep_url|

      result = {}
      tmp_res = {}

      ep_val = "<#{ep_url[:ep]}>"

      res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
        #{@@prefix}
        # epiltope id & epitope name & glycoepitope sequence
        SELECT distinct ?ep_id ?ep_name (str(?seq) AS ?sequence)
        #{@@from}
        WHERE{
          # epitope id
          ?glytoucan glycan:has_epitope #{ep_val} .
          #{ep_val} dcterms:identifier ?ep_id .

          # epitope name
          #{ep_val} rdfs:label ?ep_name .

          # glycoepitope sequence
          #{ep_val} glycan:has_glycosequence ?gseq .
          ?gseq glycan:in_carbohydrate_format glycan:carbohydrate_format_glycoepitope .
          ?gseq glycan:has_sequence ?seq .
        }
      SPARQL
      tmp_id = []
      res.each do |r|
        tmp_id.push("<a href=\"#{ep_url[:ep]}\" target=\"_blank\">#{r[:ep_id]}</a>")
        tmp_res[:ep_name] = [] if tmp_res[:ep_name].nil?
        tmp_res[:ep_name].push(r[:ep_name])
        tmp_res[:sequence] = [] if tmp_res[:sequence].nil?
        tmp_res[:sequence].push(r[:sequence])
      end
      result[:ep_tag] = tmp_id.uniq.join(', ')

      res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
        #{@@prefix}
        # glycoprotein & glycolipid
        SELECT distinct ?gp_name ?gl_name
        #{@@from}
        WHERE{
        # glycoprotein
          OPTIONAL{
            #{ep_val} dcterms:isPartOf ?gp .
            ?gp a glycan:glycoprotein .
            ?gp rdfs:label ?gp_name .
          }
          # glycolipid
          OPTIONAL{
            #{ep_val} dcterms:isPartOf ?gl .
            ?gl a glycan:glycolipid .
            ?gl rdfs:label ?gl_name .
          }
        }
      SPARQL
      res.each do |r|
        tmp_res[:gp_name] = [] if tmp_res[:gp_name].nil?
        tmp_res[:gp_name].push(r[:gp_name])
        tmp_res[:gl_name] = [] if tmp_res[:gl_name].nil?
        tmp_res[:gl_name].push(r[:gl_name])
      end

      res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
        #{@@prefix}
        # cell line & tissue and cellular distribution
        SELECT distinct ?cell_name ?tissue_name
        #{@@from}
        WHERE{
        # cell line
          OPTIONAL{
            #{ep_val} glycoepitope:found_in ?cell_line .
            ?cell_line rdfs:label ?cell_name .
          }
          # Tissue and Cellular distribution
          OPTIONAL{
            #{ep_val} glycoepitope:tissue ?tissue .
            ?tissue rdfs:label ?tissue_name .
          }
        }
      SPARQL
      res.each do |r|
        tmp_res[:cell_name] = [] if tmp_res[:cell_name].nil?
        tmp_res[:cell_name].push(r[:cell_name])
        tmp_res[:tissue_name] = [] if tmp_res[:tissue_name].nil?
        tmp_res[:tissue_name].push(r[:tissue_name])
      end

      res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
        #{@@prefix}
        #pubmed url and pubmed id
        SELECT distinct ?pubmed ?pubmed_id
        #{@@from}
        WHERE{
          # pubmed
          OPTIONAL{
            #{ep_val} dcterms:isReferencedBy ?citation .
            ?citation rdfs:seeAlso ?pubmed .
            ?pubmed dcterms:identifier ?pubmed_id .
          }
        }
      SPARQL
      tmp_pmid = []
      res.each do |r|
        tmp_pmid.push("<a href=\"#{r[:pubmed]}\" target=\"_blank\">#{r[:pubmed_id]}</a>")
      end
      result[:pubmed] = tmp_pmid.uniq.join(', ')

      res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
        #{@@prefix}
        # antibody id & antibody name
        SELECT distinct ?antibody ?anti_id ?anti_name
        #{@@from}
        WHERE{
          OPTIONAL{
            # antibody id
            #{ep_val} glycoepitope:has_antibody ?antibody .
            ?antibody dcterms:identifier ?anti_id .
          }
          OPTIONAL{
            # antibody name
            #{ep_val} glycoepitope:has_antibody ?antibody .
            ?antibody rdfs:label ?anti_name .
          }
        }
      SPARQL
      tmp_anti = []
      res.each do |r|
        tmp_anti.push("<tr><td><a href=\"#{r[:antibody]}\" target=\"_blank\">#{r[:anti_id]}</a></td><td>#{r[:anti_name]}</td></tr>")
      end
      result[:anti_tag] = tmp_anti.uniq.join("\n")

      tmp_res.each do |key,val|
        result[key] = val.uniq.join('<br />')
      end

      result[:l_epitope] = @@item_labels['epitope']
      result[:l_antibody] = @@item_labels['antibody']
      result[:l_epitopeId] = @@item_labels['epitopeId']
      result[:l_epitopeName] = @@item_labels['epitopeName']
      result[:l_sequence] = @@item_labels['sequence']
      result[:l_glycoprotein] = @@item_labels['glycoprotein']
      result[:l_glycolipid] = @@item_labels['glycolipid']
      result[:l_cellName] = @@item_labels['cellName']
      result[:l_tissueName] = @@item_labels['tissueName']
      result[:l_pubmed] = @@item_labels['pubmed']
      result[:l_antibodyId] = @@item_labels['antibodyId']
      result[:l_antibodyName] = @@item_labels['antibodyName']

      res_array.push(result)
    end

    res_array

  end
end
