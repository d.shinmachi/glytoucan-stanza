class RelationListStanza < TogoStanza::Stanza::Base
  require 'net/http'
  require 'uri'
  require 'json'

  @@prefix = [
    'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
    'PREFIX glycan:  <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>',
    'PREFIX rogs: <http://http://www.glycoinfo.org/glyco/owl/relation#>'
  ].join("\n")

  @@item_labels = {}
  @@default_labels = {
    'accessionNumber' => 'Accession Number',
    'cardinality' => 'Cardinality'
  }

  property :relSearch do |acc, category, lang|

  	uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
  	result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
  	json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
  	list = JSON.parse(json)
  	lang_replace = list['status'] == false ? {} : list['result']['common']
  	@@default_labels.each do |key,label|
  	  @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
  	end

	select_query = []
	cat_query = []
	if category == 'motif'
		select_text = '?MotifName ?moAccNum'
		cat_query.push('OPTIONAL{')
		cat_query.push('?glycan glycan:has_motif ?motif .')
		cat_query.push('?motif a glycan:glycan_motif ;')
		cat_query.push('rdfs:label ?motifLabel .')
    cat_query.push('BIND((str(?motifLabel) AS ?MotifName))')
		cat_query.push('?motif glytoucan:has_primary_id ?moAccNum .}')
    
  elsif category == 'monosaccharide'
    select_text = '?ComponentName ?cardinality'
    cat_query.push('OPTIONAL{')
    cat_query.push('?glycan glycan:has_component ?comp .')
    cat_query.push('?comp glycan:has_cardinality ?cardinality .')
    cat_query.push('?comp glycan:has_monosaccharide ?mono .')
    cat_query.push('?mono glycan:has_alias ?msdb .')
    cat_query.push('?msdb glycan:has_alias_name ?ComponentName .}')

  elsif category == 'linkage isomer'
    select_text = '?isomerAccNum'
    cat_query.push('OPTIONAL{')
    cat_query.push('?glycan rogs:hasLinkageIsomer ?isomer .')
    cat_query.push('?isomer glytoucan:has_primary_id ?isomerAccNum .}')
	end
	query = cat_query.join("\n")

    relation_list = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    #{@@prefix}
		SELECT DISTINCT #{select_text}
    FROM <http://rdf.glytoucan.org/core>
		FROM <http://rdf.glytoucan.org/ms/carbbank>
		FROM <http://rdf.glytoucan.org/motif>
    FROM <http://rdf.glytoucan.org/isomer>

		WHERE{
			# Accession Number
			?glycan a glycan:saccharide.
			?glycan glytoucan:has_primary_id "#{acc}" .
			#{query}
		}
    SPARQL

    if category == 'motif'
      freq_list = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)

        #{@@prefix}

        SELECT DISTINCT ?MotifName (count(distinct ?glycan) AS ?Frequency)
        FROM <http://rdf.glytoucan.org/core>
        FROM <http://rdf.glytoucan.org/motif>

        WHERE{
          ?glycan glycan:has_motif ?motif.
          ?motif rdfs:label ?motifLabel .
          BIND((str(?motifLabel) AS ?MotifName))
        }
        GROUP BY ?MotifName
        ORDER BY DESC(count(distinct ?glycan))
      SPARQL
      motif_freq = {}
      freq_list.each do |entry|
        motif_freq[entry[:MotifName]] = entry[:Frequency].to_i
      end
      relation_list.sort!{|x,y|
        motif_freq[y[:MotifName]] <=> motif_freq[x[:MotifName]]
      }
    end

    relation_list.map{|entry|
      entry[:l_accessionNumber] = @@item_labels['accessionNumber']
      entry[:l_cardinality] = @@item_labels['cardinality']
      entry
    }

  end

end
